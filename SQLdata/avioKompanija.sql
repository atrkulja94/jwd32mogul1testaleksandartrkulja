DROP SCHEMA IF EXISTS avioKompanija;
CREATE SCHEMA avioKompanija DEFAULT CHARACTER SET utf8;
USE avioKompanija;

    
CREATE TABLE letovi (
	id INT NOT NULL AUTO_INCREMENT,
	brLeta INT UNIQUE NOT NULL,
    datumPoletanja DATETIME NOT NULL,
    polaziste VARCHAR(20) NOT NULL,
    datumSletanja DATETIME NOT NULL,
    odrediste VARCHAR(20) NOT NULL,
   	sedistaEkonomska INT NOT NULL, 
   	sedistaBiznis INT NOT NULL,
   	
    PRIMARY KEY(id)
);

CREATE TABLE karta(
	id INT NOT NULL AUTO_INCREMENT,
	klasa VARCHAR(20) NOT NULL,
	brojKarte INT NOT NULL,
	datumProdaje DATETIME NOT NULL,
	cena DOUBLE NOT NULL,
	kupac VARCHAR(30) NOT NULL,
	let INT NOT NULL,
	
	PRIMARY KEY(id),
	FOREIGN KEY(let) REFERENCES letovi(id)
    ON DELETE RESTRICT
);

INSERT INTO letovi (id, brLeta, datumPoletanja, polaziste, datumSletanja, odrediste, sedistaEkonomska, sedistaBiznis) VALUES (1, 10, '2017-09-01 01:00:00', 'Pariz', '2017-09-01 03:00:00', 'Berlin', 100, 50);
INSERT INTO letovi (id, brLeta, datumPoletanja, polaziste, datumSletanja, odrediste, sedistaEkonomska, sedistaBiznis) VALUES (2, 14, '2017-10-04 05:00:00', 'Frankfurt', '2017-10-05 01:00:00', 'Sidnej', 150, 100);
INSERT INTO letovi (id, brLeta, datumPoletanja, polaziste, datumSletanja, odrediste, sedistaEkonomska, sedistaBiznis) VALUES (3, 17, '2017-10-08 06:00:00', 'Rim', '2017-10-08 09:00:00', 'Madrid', 120, 60);

INSERT INTO karta (id, klasa, brojKarte, datumProdaje, cena, kupac, let) VALUES (1, 'Ekonomska', 1, '2017-08-09 00:03:00', 200.00, 'Milan Milic', 1);
INSERT INTO karta (id, klasa, brojKarte, datumProdaje, cena, kupac, let) VALUES (2, 'Biznis', 2, '2017-08-08 00:20:00', 350.00, 'Vuk Vukovic', 1);
INSERT INTO karta (id, klasa, brojKarte, datumProdaje, cena, kupac, let) VALUES (3, 'Ekonomska', 1, '2017-10-02 01:0:00', 600.00, 'Aca Nikolic', 2);
INSERT INTO karta (id, klasa, brojKarte, datumProdaje, cena, kupac, let) VALUES (4, 'Ekonomska', 1, '2017-10-03 08:03:00', 250.00, 'Jovana Jovanic', 3);
INSERT INTO karta (id, klasa, brojKarte, datumProdaje, cena, kupac, let) VALUES (5, 'Biznis', 2, '2017-10-05 05:03:00', 400.00, 'Marica Macic', 3);