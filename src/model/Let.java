package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Let {

	protected int id;
	protected int brLeta;
	protected Date datumPoletanja;
	protected String polaziste;
	protected Date datumSletanja;
	protected String odrediste;
	protected int brSedistaEkon;
	protected int brSedistaBiznis;
	
	List<Karta> karte = new ArrayList<>();

	public Let(int id, int brLeta, Date datumPoletanja, String polaziste, Date datumSletanja, String odrediste,
			int brSedistaEkon, int brSedistaBiznis) {
		this.id = id;
		this.brLeta = brLeta;
		this.datumPoletanja = datumPoletanja;
		this.polaziste = polaziste;
		this.datumSletanja = datumSletanja;
		this.odrediste = odrediste;
		this.brSedistaEkon = brSedistaEkon;
		this.brSedistaBiznis = brSedistaBiznis;
	}

	public Let(int brLeta, Date datumPoletanja, String polaziste, Date datumSletanja, String odrediste,
			int brSedistaEkon, int brSedistaBiznis) {
		this.id = 0;
		this.brLeta = brLeta;
		this.datumPoletanja = datumPoletanja;
		this.polaziste = polaziste;
		this.datumSletanja = datumSletanja;
		this.odrediste = odrediste;
		this.brSedistaEkon = brSedistaEkon;
		this.brSedistaBiznis = brSedistaBiznis;
	}

	@Override
	public String toString() {
		return "Let [id=" + id + ", brLeta=" + brLeta + ", datumPoletanja=" + datumPoletanja + ", polaziste="
				+ polaziste + ", datumSletanja=" + datumSletanja + ", odrediste=" + odrediste + ", brSedistaEkon="
				+ brSedistaEkon + ", brSedistaBiznis=" + brSedistaBiznis + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Let other = (Let) obj; 		
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBrLeta() {
		return brLeta;
	}

	public void setBrLeta(int brLeta) {
		this.brLeta = brLeta;
	}

	public Date getDatumPoletanja() {
		return datumPoletanja;
	}

	public void setDatumPoletanja(Date datumPoletanja) {
		this.datumPoletanja = datumPoletanja;
	}

	public String getPolaziste() {
		return polaziste;
	}

	public void setPolaziste(String polaziste) {
		this.polaziste = polaziste;
	}

	public Date getDatumSletanja() {
		return datumSletanja;
	}

	public void setDatumSletanja(Date datumSletanja) {
		this.datumSletanja = datumSletanja;
	}

	public String getOdrediste() {
		return odrediste;
	}

	public void setOdrediste(String odrediste) {
		this.odrediste = odrediste;
	}

	public int getBrSedistaEkon() {
		return brSedistaEkon;
	}

	public void setBrSedistaEkon(int brSedistaEkon) {
		this.brSedistaEkon = brSedistaEkon;
	}

	public int getBrSedistaBiznis() {
		return brSedistaBiznis;
	}

	public void setBrSedistaBiznis(int brSedistaBiznis) {
		this.brSedistaBiznis = brSedistaBiznis;
	}

	public List<Karta> getKarte() {
		return karte;
	}

	public void setKarte(List<Karta> karte) {
		this.karte = karte;
	}
	
	
	
	
}
