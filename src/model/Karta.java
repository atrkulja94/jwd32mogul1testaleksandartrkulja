package model;

import java.util.Date;

public class Karta {
	
	protected int id;
	protected String klasa;
	protected int brKarte;
	protected Date datumProdaje;
	protected double cena;
	protected String kupac;
	
	protected Let let;
	
	public Karta(int id, String klasa, int brKarte, Date datumProdaje, double cena, String kupac, Let let) {
		this.id = id;
		this.klasa = klasa;
		this.brKarte = brKarte;
		this.datumProdaje = datumProdaje;
		this.cena = cena;
		this.kupac = kupac;
		this.let = let;
	}
	
	public Karta(String klasa, int brKarte, Date datumProdaje, double cena, String kupac, Let let) {
		this.id = 0;
		this.klasa = klasa;
		this.brKarte = brKarte;
		this.datumProdaje = datumProdaje;
		this.cena = cena;
		this.kupac = kupac;
		this.let = let;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Karta other = (Karta) obj; 		
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Karta [id=" + id + ", klasa=" + klasa + ", brKarte=" + brKarte + ", datumProdaje=" + datumProdaje
				+ ", cena=" + cena + ", kupac=" + kupac + ", let=" + let.getBrLeta() + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKlasa() {
		return klasa;
	}

	public void setKlasa(String klasa) {
		this.klasa = klasa;
	}

	public int getBrKarte() {
		return brKarte;
	}

	public void setBrKarte(int brKarte) {
		
		this.brKarte = brKarte;
	}

	public Date getDatumProdaje() {
		return datumProdaje;
	}

	public void setDatumProdaje(Date datumProdaje) {
		this.datumProdaje = datumProdaje;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public String getKupac() {
		return kupac;
	}

	public void setKupac(String kupac) {
		this.kupac = kupac;
	}

	public Let getLet() {
		return let;
	}

	public void setLet(Let let) {
		this.let = let;
	}
	
	

}
