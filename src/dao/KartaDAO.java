package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mysql.cj.result.SqlDateValueFactory;

import model.Karta;
import model.Let;
import utils.PomocnaKlasa;

public class KartaDAO {

	public static List<Karta> getByIdLeta(Connection conn, int id) {
		List<Karta> sveKarte = new ArrayList<>();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, klasa, brojKarte, datumProdaje, cena, kupac FROM karta WHERE let = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			rset = pstmt.executeQuery();
			
			while(rset.next()) {
				int index = 1;
				int idKarte = rset.getInt(index++);
				String klasa = rset.getString(index++);
				int brojKarte = rset.getInt(index++);
				Date datumProdaje = PomocnaKlasa.sdf.parse(rset.getString(index++));
				double cena = rset.getDouble(index++);
				String kupac = rset.getString(index++);
				
				Let let = LetDAO.getById(conn, id);
				Karta karta = new Karta(idKarte, klasa, brojKarte, datumProdaje, cena, kupac, let);
				sveKarte.add(karta);
			}
		} catch (Exception e) {
			System.out.println("greska u upitu.");
			e.printStackTrace();
		}finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return sveKarte;
	}

	public static boolean add(Connection conn, Karta karta) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO karta (klasa, brojKarte, datumProdaje, cena, kupac, let) VALUES (?, ?, ?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, karta.getKlasa());
			pstmt.setInt(index++, karta.getBrKarte());
			pstmt.setString(index++, PomocnaKlasa.sdf.format(karta.getDatumProdaje()));
			pstmt.setDouble(index++, karta.getCena());
			pstmt.setString(index++, karta.getKupac());
			pstmt.setInt(index++, karta.getLet().getId());
			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
		
	}

}
