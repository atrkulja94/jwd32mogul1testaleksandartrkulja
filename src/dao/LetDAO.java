package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Let;
import utils.PomocnaKlasa;

public class LetDAO {

	public static List<Let> getAll(Connection conn) {
		List<Let> sviLetovi = new ArrayList<>();
		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, brLeta, datumPoletanja, polaziste, datumSletanja, odrediste, sedistaEkonomska, sedistaBiznis FROM letovi";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int brLeta = rset.getInt(index++);
				Date poletanje = PomocnaKlasa.sdf.parse(rset.getString(index++));
				String polaziste = rset.getString(index++);
				Date sletanje = PomocnaKlasa.sdf.parse(rset.getString(index++));
				String odrediste = rset.getString(index++);
				int sedistaEk = rset.getInt(index++);
				int sedistaBiz = rset.getInt(index++);
				
				Let let = new Let(id, brLeta, poletanje, polaziste, sletanje, odrediste, sedistaEk, sedistaBiz);
				sviLetovi.add(let);
			}
		} catch (Exception ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return sviLetovi;
	}

	public static Let getByBrLeta(Connection conn, int brojLeta) {
		Let let = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, datumPoletanja, polaziste, datumSletanja, odrediste, sedistaEkonomska, sedistaBiznis FROM letovi WHERE brLeta = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, brojLeta);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				Date poletanje = PomocnaKlasa.sdf.parse(rset.getString(index++));
				String polaziste = rset.getString(index++);
				Date sletanje = PomocnaKlasa.sdf.parse(rset.getString(index++));
				String odrediste = rset.getString(index++);
				int sedistaEk = rset.getInt(index++);
				int sedistaBiz = rset.getInt(index++);
				
				let = new Let(id, brojLeta, poletanje, polaziste, sletanje, odrediste, sedistaEk, sedistaBiz);
				
			}
		} catch (Exception e) {
			System.out.println("greska u upitu.");
			e.printStackTrace();
		}finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		
		return let;
	}

	public static Let getById(Connection conn, int id) {
		Let let = null;
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT brLeta, datumPoletanja, polaziste, datumSletanja, odrediste, sedistaEkonomska, sedistaBiznis FROM letovi WHERE id = ?";
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, id);
			rset = pstmt.executeQuery();
			
			if(rset.next()) {
				int index = 1;
				int brojLeta = rset.getInt(index++);
				Date poletanje = PomocnaKlasa.sdf.parse(rset.getString(index++));
				String polaziste = rset.getString(index++);
				Date sletanje = PomocnaKlasa.sdf.parse(rset.getString(index++));
				String odrediste = rset.getString(index++);
				int sedistaEk = rset.getInt(index++);
				int sedistaBiz = rset.getInt(index++);
				
				let = new Let(id, brojLeta, poletanje, polaziste, sletanje, odrediste, sedistaEk, sedistaBiz);
				
			}
		} catch (Exception e) {
			System.out.println("greska u upitu.");
			e.printStackTrace();
		}finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		
		return let;
	}

}
