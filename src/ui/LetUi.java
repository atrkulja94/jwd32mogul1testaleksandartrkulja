package ui;

import java.util.ArrayList;
import java.util.List;

import dao.KartaDAO;
import dao.LetDAO;
import model.Karta;
import model.Let;
import utils.PomocnaKlasa;

public class LetUi {

	public static void prikazSvihLetova() {
		
		List<Let> letovi = LetDAO.getAll(ApplicationUI.getConn());
		System.out.println("=========================================================");
		System.out.printf("%4s %4s %25s %12s %25s %12s %5s %5s\n", "id", "brLeta", "Poletanje", "polaziste", "Sletanje", "odrediste", "sedista ek", "sedista Biz");
		System.out.println("=========================================================");
		for (Let let: letovi) {
			System.out.printf("%4s %4s %25s %12s %25s %12s %5s %5s\n", let.getId(), let.getBrLeta(),
					PomocnaKlasa.sdf.format(let.getDatumPoletanja()), let.getPolaziste(), PomocnaKlasa.sdf.format(let.getDatumSletanja()), let.getOdrediste(),
					let.getBrSedistaEkon(), let.getBrSedistaBiznis());
		}
		System.out.println("=========================================================");
	}

	public static void prikazLetaSaKartama() {

		Let let = pronadjiLet();
		if(let == null) {
			System.out.println("ne postoji let sa unetim brojem leta.");
		}else {
			List<Karta> karte = KartaDAO.getByIdLeta(ApplicationUI.getConn(), let.getId());
			
			System.out.println("==================================================================================================================");
			System.out.printf("%4s %4s %25s %12s %25s %12s %5s %5s\n", "id", "brLeta", "Poletanje", "polaziste", "Sletanje", "odrediste", "sedista ek", "sedista Biz");
			System.out.println("==================================================================================================================");
			System.out.printf("%4s %4s %25s %12s %25s %12s %5s %5s\n", let.getId(), let.getBrLeta(),
					PomocnaKlasa.sdf.format(let.getDatumPoletanja()), let.getPolaziste(), PomocnaKlasa.sdf.format(let.getDatumSletanja()), let.getOdrediste(),
					let.getBrSedistaEkon(), let.getBrSedistaBiznis());
			System.out.println("==================================================================================================================");
			for(Karta karta : karte) {
				System.out.println(karta);System.out.println();
			}
		}
		
		
	}
	
	public static Let pronadjiLet() {
		System.out.print("Unesite broj leta:");
		int brojLeta = PomocnaKlasa.ocitajCeoBroj();
		
		Let let = LetDAO.getByBrLeta(ApplicationUI.getConn(), brojLeta);
		
			return let;

	}
	
	

}
