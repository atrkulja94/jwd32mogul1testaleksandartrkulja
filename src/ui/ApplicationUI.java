package ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import model.Let;
import utils.PomocnaKlasa;

public class ApplicationUI {

private static Connection conn;
	
	static {
	
		try {
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/avioKompanija?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();
			System.exit(0);
		}
	}
	
	public static void main(String[] args) {
		
		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				LetUi.prikazSvihLetova();
				break;
			case 2:
				KartaUi.prodajaKarte();
				break;
			case 3:
				LetUi.prikazLetaSaKartama();
				break;
//			case 4:
//				
//				break;
//			case 5:
//				
//				break;
			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}

		try {
			conn.close();
		} catch (SQLException ex) {
		}
		
		
		

	}

	private static void ispisiMenu() {
		System.out.println("Banka - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - prikaz svih letova");
		System.out.println("\tOpcija broj 2 - prodaja karte");
		System.out.println("\tOpcija broj 3 - prikaz odredjenog leta sa svim kartama");

		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
		
	}
	
	public static Connection getConn() {
		return conn;
	}

}
