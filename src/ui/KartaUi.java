package ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import dao.KartaDAO;
import dao.LetDAO;
import model.Karta;
import model.Let;
import utils.PomocnaKlasa;

public class KartaUi {

	public static void prodajaKarte() {
		System.out.println("unesite broj leta.");
		int brLeta = PomocnaKlasa.ocitajCeoBroj();
		Let let = LetDAO.getByBrLeta(ApplicationUI.getConn(), brLeta);
		if(let != null) {
			List<Karta> karte = KartaDAO.getByIdLeta(ApplicationUI.getConn(), let.getId());
			
			String klasa = "";
			while(!klasa.equalsIgnoreCase("Biznis") && !klasa.equalsIgnoreCase("ekonomska")) {
				System.out.println("unesite klasu karte.");
				klasa = PomocnaKlasa.ocitajTekst();
			}
			
			int brojac = 0;
			boolean pronadjeno = true;
			int BRkarte = 0;
			
			
			if(klasa.equalsIgnoreCase("Biznis")){
			while (pronadjeno == true) {
				
				brojac = 0;
				
				List<Karta> biznisKarte = new ArrayList<>();
				for(Karta k : karte) {
					if(k.getKlasa().equalsIgnoreCase("biznis")) {
						biznisKarte.add(k);
					}
				}
				
				System.out.println("unesite broj karte.");
				int brKarte = PomocnaKlasa.ocitajCeoBroj();
				
			for(int i = 0; i<biznisKarte.size(); i++) {
			
				if(biznisKarte.get(i).getBrKarte() == brKarte) {
					System.out.println("karta sa tim brojem vec postoji na trazenom letu u biznis klasi.");
					pronadjeno = true;
					brojac +=1;
				}
				
				if(brKarte > let.getBrSedistaBiznis() ) {
					System.out.println("Br karte mora biti u opsegu od 1 do " + let.getBrSedistaBiznis() + " .");
					pronadjeno = true;
					brojac +=1;
					break;
				}
			
			}
			if(brojac == 0) {
				BRkarte = brKarte;
				pronadjeno = false;
			}
			}
			}
			
			if(klasa.equalsIgnoreCase("Ekonomska")) {
			while(pronadjeno == true) {
				
				brojac = 0;
				
				List<Karta> ekonomskaKarte = new ArrayList<>();
				for(Karta k : karte) {
					if(k.getKlasa().equalsIgnoreCase("ekonomska")) {
						ekonomskaKarte.add(k);
					}
				}
				
				System.out.println("unesite broj karte.");
				int brKarte = PomocnaKlasa.ocitajCeoBroj();
				
				for(Karta k : ekonomskaKarte) {
					if(k.getBrKarte() == brKarte) {
						System.out.println("karta sa tim brojem vec postoji na trazenom letu u ekonomskoj klasi.");
						pronadjeno = true;
						brojac +=1;
					}
					if(brKarte > let.getBrSedistaEkon()) {
						System.out.println("Br karte mora biti u opsegu od 1 do " + let.getBrSedistaEkon() + " .");
						pronadjeno = true;
						brojac +=1;
						break;
					}
						
				}
				if(brojac == 0) {
					BRkarte = brKarte;
					pronadjeno = false;	
				}
				}	
			}
			
			System.out.println("unesite kupca.");
			String kupac = PomocnaKlasa.ocitajTekst();
			Date datumProdaje = null;
			int razlika = 50;   
			while(razlika > 28) {
				
			datumProdaje = PomocnaKlasa.ocitajDatum();	
			razlika = (int)( (let.getDatumPoletanja().getTime() - datumProdaje.getTime()) 
		                 / (1000 * 60 * 60 * 24) );
				
//			long datumprodaje = datumProdaje.getTime();
//			long datumpoletanja = let.getDatumPoletanja().getTime();
//			razlika = datumpoletanja - datumprodaje ;
			}
			
			System.out.println("unesite cenu.");
			double cena = PomocnaKlasa.ocitajRealanBroj();
			
			Karta karta = new Karta(klasa, BRkarte, datumProdaje, cena, kupac, let);
			KartaDAO.add(ApplicationUI.getConn(), karta);
			
		} else {
			System.out.println("trazeni let ne postoji.");
			return;
		}
			
		
	}


	
	}

